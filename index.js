/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
function userInformation (){
	let name = prompt ("What is your name? ");
	let age = prompt ("How old are you? ");
	let location = prompt ("Where are you located? ");
	alert("Thank you for your input!");
	
	console.log(name)
	console.log(age)
	console.log(location)
}
	userInformation();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favoriteBands(){
		let band1 = "1. The Beatles"
		let band2 = "2. Metallica"
		let band3 = "3. The Eagles"
		let band4 = "4. Queen"
		let band5 = "5. Pink Floyd"

		console.log(band1);
		console.log(band2);
		console.log(band3);
		console.log(band4);
		console.log(band5);
}
	favoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function favoriteMovies() {
		let movie1 = "1. The God father"
		let rating1 = "Rotten tomatoes rating is 97%"
		console.log(movie1);
		console.log(rating1);

		let movie2 = "2. The God father II"
		let rating2 = "Rotten tomatoes rating is 96%"
		console.log(movie2);
		console.log(rating2);

		let movie3 = "3. Shawshank Redemption"
		let rating3 = "Rotten tomatoes rating is 91%"
		console.log(movie3);
		console.log(rating3);

		let movie4 = "4. To Kill a Mockingbird"
		let rating4 = "Rotten tomatoes rating is 93%"
		console.log(movie4);
		console.log(rating4);

		let movie5 = "5. Psycho"
		let rating5 = "Rotten tomatoes rating is 96%"
		console.log(movie5);
		console.log(rating5);
	}

	favoriteMovies();




/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
}
printUsers();
